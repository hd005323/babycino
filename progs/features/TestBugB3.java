class TestBugB3 {
    public static void main(String[] a) {
	System.out.println(new Test().xy());
    }
}

class Test {

    public boolean xy(){
		boolean result;
		boolean x = true;
		boolean y = false;
		boolean z = true;
		if (x != y && z){
			result = true; //!= has higher precedence (xy!=z&&)
		}else{
			result = false; //&& has higher precedence(xyz&&!=)
		}
	}
	return result;
    }

}

